#!/bin/bash
mount -o bind /dev $1/dev > /dev/null
mount -t proc none $1/proc > /dev/null
mount -o bind /sys $1/sys > /dev/null
mount -o bind /sys/firmware/efi/efivars
mount -o bind /tmp $1/tmp > /dev/null
mount -o bind /run $1/run > /dev/null
chroot $1 /bin/bash
umount -f $1/dev > /dev/null
umount -f $1/proc > /dev/null
umount -f /sys/firmware/efi/efivars
umount -f $1/sys > /dev/null
umount -f $1/tmp > /dev/null
umount -f $1/run > /dev/null
