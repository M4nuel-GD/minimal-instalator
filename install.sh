#!/bin/bash
#apt install -y debootstrap arch-install-scripts
mkfs.fat -F32 /dev/sda1
mkfs.ext4 /dev/sda2
mount /dev/sda2 /mnt
mkdir /mnt/boot
mkdir /mnt/boot/EFI
mount /dev/sda1 /mnt/boot/EFI
mkdir /tmp/apt-de
debootstrap --arch=amd64 --variant=minbase --cache-dir=/tmp/apt-de kali-rolling /mnt
genfstab -U /mnt >> /mnt/etc/fstab
echo -ne "LANG=es_US.UTF-8\nLANGUAGE=es_US.UTF-8\nLC_ALL=es_US.UTF-8" > /mnt/etc/default/locale
# dpkg-reconfigure console-setup
# dpkg-reconfigure tzdata
# dpkg-reconfigure keyboard-configuration
# dpkg-reconfigure locales
# locale-gen es_US.UTF-8
# grub-install --target=x86_64-efi --efi-directory=/boot/EFI --bootloader-id=GRUB
# grub-mkconfig -o /boot/grub/grub.cfg
# useradd -m -G grupo1,grupo2 -s /bin/bash manuel
