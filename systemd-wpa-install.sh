#!/bin/bash
systemctl enable systemd-networkd
systemctl enable systemd-resolved
mv /etc/resolv.conf /etc/resolv.conf.backup
ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
echo -e "[Match]\nName=en*\n\n[Network]\nDHCP=ipv4" >> /etc/systemd/network/wired.network
echo -e "[Match]\nName=$(ls /sys/class/net/ | grep w)\n\n[Network]\nDHCP=ipv4" >> /etc/systemd/network/wl.network
systemctl enable wpa_supplicant@$(ls /sys/class/net/ | grep w)
echo -e "ctrl_interface=/run/wpa_supplicant/\nctrl_interface_group=netdev\nupdate_config=1" >> /etc/wpa_supplicant/wpa_supplicant-$(ls /sys/class/net/ | grep w).conf
